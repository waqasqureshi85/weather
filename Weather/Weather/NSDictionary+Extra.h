//
//  NSDictionary+Extra.h
//  Weather
//
//  Created by Waqas Quershi on 28/02/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extra)

- (NSString *)stringForKey:(NSString *)key;

@end
