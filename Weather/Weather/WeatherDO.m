//
//  WeatherDO.m
//  Weather
//
//  Created by Waqas Qureshi on 14/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "WeatherDO.h"
#import "NSDictionary+Extra.h"
@implementation WeatherDO
@synthesize city, country, description, current_temp,temp_max,temp_min,weather_icon;

#pragma ############-- Parser --############
- (instancetype)initWithAttributes:(NSDictionary *)data
{
    if (data == nil) return nil;
    if ((self = [super init])) {
        self.city = [data stringForKey:@"name"];
        
        NSDictionary *sys = [data objectForKey:@"sys"];
        self.country = [sys stringForKey:@"country"];
        
        NSDictionary *main = [data objectForKey:@"main"];
        self.current_temp = [self tempInFahrenheit:[[main stringForKey:@"temp"] floatValue]];
        self.temp_max = [self tempInFahrenheit:[[main stringForKey:@"temp_max"] floatValue]];
        self.temp_min = [self tempInFahrenheit:[[main stringForKey:@"temp_min"] floatValue]];
        
        NSDictionary *weather = [[data objectForKey:@"weather"] objectAtIndex:0];
        self.description = [weather stringForKey:@"description"];
        self.weather_icon = [NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",[weather stringForKey:@"icon"]];

    }
    return self;
}

#pragma ############-- Methods --############
- (float ) tempInCelcius:(float) temp
{
    return (temp - 273.15);
}
- (float ) tempInFahrenheit:(float) temp
{
    return ((temp * 9/5) - 459.67);
}
@end
