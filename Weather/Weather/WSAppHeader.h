//
//  WSAppHeader.h
//  Weather
//
//  Created by Waqas Qureshi on 14/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#ifndef WSAppHeader_h
#define WSAppHeader_h

#define SERV_URL @"http://api.openweathermap.org/data/2.5/"
#define WEATHER_API_KEY @"5b439e91733b83da9acbe38b30e1a3f2"
#define WEATHER_BASE_URL @"http://api.openweathermap.org/data/"
#define WEATHER_API_VER @"2.5"
#endif /* WSAppHeader_h */
