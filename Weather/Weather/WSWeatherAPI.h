//
//  WSWeatherAPI.h
//  Weather
//
//  Created by Waqas Qureshi on 15/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSAppHeader.h"
#import <AFNetworking/AFNetworking.h>
#import "WeatherDO.h"

@interface WSWeatherAPI : NSObject
+ (WSWeatherAPI *)getWeatherAPI;

-(void) currentWeatherByCityName:(NSString *) name
                    withCallback:( void (^)( NSError* error, WeatherDO *result ) )callback;

@end
