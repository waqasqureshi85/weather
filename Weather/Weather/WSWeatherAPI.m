//
//  WSWeatherAPI.m
//  Weather
//
//  Created by Waqas Qureshi on 15/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "WSWeatherAPI.h"
#import <Foundation/Foundation.h>
@interface WSWeatherAPI () {
    NSString *_baseURL;
    NSString *_apiKey;
    NSString *_apiVersion;
}
-(void) callWeatherAPIMethod:(NSString *) method
                withCallback:( void (^) ( NSError *error, WeatherDO *result) )callback;
@end

@implementation WSWeatherAPI

+ (WSWeatherAPI *)getWeatherAPI
{
    static WSWeatherAPI *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
        [_sharedInstance defaultAPISettings];
    });
    
    return _sharedInstance;
}

-(void)defaultAPISettings
{
    _baseURL = WEATHER_BASE_URL;
    _apiKey = WEATHER_API_KEY;
    _apiVersion = WEATHER_API_VER;
}

#pragma ############-- API Methods --############
-(void) callWeatherAPIMethod:(NSString *) method
                withCallback:( void (^) ( NSError *error, WeatherDO *result) )callback
{
//    [AFNetworkReachabilityManager sharedManager]
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:[NSString stringWithFormat:@"%@%@%@&APPID=%@",_baseURL,_apiVersion,method,_apiKey] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *weatherData = (NSDictionary *)responseObject;
        WeatherDO *weatherInfo = [[WeatherDO alloc] initWithAttributes:weatherData];
        callback(nil,weatherInfo);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        callback(error,nil);
    }];
}

-(void) currentWeatherByCityName:(NSString *) city
                    withCallback:( void (^)( NSError* error, WeatherDO *result ) )callback
{
    NSString *encodedString = [city stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSString *method = [NSString stringWithFormat:@"/weather?q=%@", encodedString];
    [self callWeatherAPIMethod:method withCallback:callback];
}
@end
