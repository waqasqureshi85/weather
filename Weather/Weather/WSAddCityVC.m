//
//  WSAddCityVC.m
//  Weather
//
//  Created by Waqas Qureshi on 15/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "WSAddCityVC.h"

@interface WSAddCityVC ()
@property (weak, nonatomic) IBOutlet UITextField *cityTxtField;

@end

@implementation WSAddCityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_cityTxtField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelAction:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)addCityAction:(id)sender {
    NSString *city = _city_tfield.text;
    
    if (city && [city length]>0) {
        [_city_tfield resignFirstResponder];
        [_delegate didAddNewCity:city];
    }
    _delegate = nil;

    [self.navigationController dismissViewControllerAnimated:YES completion:^{
            }];
}
@end
