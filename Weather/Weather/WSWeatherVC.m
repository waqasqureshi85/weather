//
//  WSWeatherVC.m
//  Weather
//
//  Created by Waqas Qureshi on 15/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "WSWeatherVC.h"
#import "WSWeatherAPI.h"
#import "WeatherDO.h"
#import <UIImageView+AFNetworking.h>

@interface WSWeatherVC () <WSAddCityDelegate>
{
    WeatherDO *weatherInfo;
}
- (IBAction)addCityViewTransition:(id)sender;
@end

@implementation WSWeatherVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Fetch last saved city name
    NSString *city = [[NSUserDefaults standardUserDefaults] objectForKey:@"city"];
    if(city && [city length]>0)
        [self fetchWeatherInfoByCity:city];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void) fetchWeatherInfoByCity:(NSString *)city
{
    [[WSWeatherAPI getWeatherAPI] currentWeatherByCityName:city withCallback:^(NSError *error, WeatherDO *result) {
        if(result)
        {
            weatherInfo = result;
            
            // show details on screen
            [self populateWeatherInfo];
        }else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                           message:@"Test Message" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:NO completion:nil];
        }
    }];
}

-(void)populateWeatherInfo
{
    // Saved city name
    [[NSUserDefaults standardUserDefaults] setObject:weatherInfo.city forKey:@"city"];
    
    self.weather_desc_lbl.text = weatherInfo.description;
    self.city_lbl.text = [NSString stringWithFormat:@"%@,%@",weatherInfo.city,weatherInfo.country];
    
    self.current_temp_lbl.text =  [NSString stringWithFormat:@"%d",(int)ceilf(weatherInfo.current_temp)];
    if (weatherInfo.weather_icon)
        [self.weather_icon setImageWithURL:[NSURL URLWithString:weatherInfo.weather_icon]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma ############-- IBAction(s) --############

- (IBAction)addCityViewTransition:(id)sender {
    [self performSegueWithIdentifier:@"WSAddCityIdentifier" sender:self];
}

#pragma ############-- Navigation --############
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"WSAddCityIdentifier"]) {
        UINavigationController *addCityNavCont = (UINavigationController *)[segue destinationViewController];
        WSAddCityVC *addCityVC =  (WSAddCityVC *)[addCityNavCont.viewControllers objectAtIndex:0];
        addCityVC.delegate = self;
    }
}

#pragma mark- ############-- Add New City Delegate --############
-(void) didAddNewCity:(NSString *)city
{
    [self fetchWeatherInfoByCity:city];
}

@end
