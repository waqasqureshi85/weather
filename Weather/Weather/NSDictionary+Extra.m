//
//  NSDictionary+Extra.m
//  Weather
//
//  Created by Waqas Quershi on 28/02/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import "NSDictionary+Extra.h"

@implementation NSDictionary (Extra)

- (NSString *)stringForKey:(NSString *)key
{
    if ([self objectForKey:key])
    {
        return [NSString stringWithFormat:@"%@", [self objectForKey:key]];
    }
    return @"";
}

@end
