//
//  WeatherDO.h
//  Weather
//
//  Created by Waqas Qureshi on 14/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherDO : NSObject

@property (strong, nonatomic) NSString      *city;
@property (strong, nonatomic) NSString      *country;
@property (readwrite, nonatomic) float      current_temp;
@property (readwrite, nonatomic) float      temp_max;
@property (readwrite, nonatomic) float      temp_min;
@property (strong, nonatomic) NSString      *description;
@property (strong, nonatomic) NSString      *weather_icon;

- (instancetype)initWithAttributes:(NSDictionary *)data;

- (float ) tempInCelcius:(float) temp;
- (float ) tempInFahrenheit:(float) temp;
@end
