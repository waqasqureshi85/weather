//
//  WSWeatherVC.h
//  Weather
//
//  Created by Waqas Qureshi on 15/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSAddCityVC.h"

@interface WSWeatherVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *weather_icon;
@property (weak, nonatomic) IBOutlet UILabel *weather_desc_lbl;
@property (weak, nonatomic) IBOutlet UILabel *city_lbl;
@property (weak, nonatomic) IBOutlet UILabel *current_temp_lbl;

@end

