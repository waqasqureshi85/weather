//
//  WSAddCityVC.h
//  Weather
//
//  Created by Waqas Qureshi on 15/11/2016.
//  Copyright © 2016 Virtusa Polaris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSWeatherVC.h"

@protocol WSAddCityDelegate <NSObject>

-(void)didAddNewCity:(NSString *)city;
@end

@interface WSAddCityVC : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *city_tfield;
@property (nonatomic, strong) id <WSAddCityDelegate> delegate;

- (IBAction)cancelAction:(id)sender;
- (IBAction)addCityAction:(id)sender;

@end
