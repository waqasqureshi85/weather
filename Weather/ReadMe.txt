README.txt
 If you find any omissions or errors in this document please notify me on this waqasqureshi08@gmail.com.

NOTE: 

Installation
------------
There’s nothing you need to download as a pre-requisite. Just go to Weather/ and open “Weather.xcworkspace” in Xcode - Version 8.0 (8A218a). 
Compile and Run.

Directory Structure:
-------------------
AppDelegate - Contains AppDelegate.

ViewControllers - contains all view controllers of project.

Weather API Controller - It’s contain a singleton controller which performing all API calls as a centralise hub.

WSConstant - It contains application constants.

Pods - It contains third-party controls and configuration.

Utilities - It contains different categories of native classes. The purpose of this module is to enhance the functionality of native classes.

Data Objects - It contains data model.

###
